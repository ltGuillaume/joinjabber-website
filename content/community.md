+++
title = "Community"
+++

We are a new collective who would like to help improve user experience in the Jabber/XMPP ecosystem. You can read more about [our goals](@/collective/about/goals/index.md), or read our last [meeting minutes](@/collective/_index.md). We don't have any form of formal membership, so if you feel like we have some common interests, your participation is welcome!

Would you like to get in touch with us? Contribute to the project? We have different chatrooms you can join. For the moment, all of these chatrooms are in English, although we welcome initiatives to create new ones for other languages:

- [chat@joinjabber.org (web chat)](https://anon.joinjabber.org/#/guest?join=chat%40joinjabber.org) ([xmpp](xmpp:chat@joinjabber.org?join)): a general chatroom for the collective, to ask questions and get involved
- [abuse@joinjabber.org (web chat)](https://anon.joinjabber.org/#/guest?join=abuse%40joinjabber.org) ([xmpp](xmpp:abuse@joinjabber.org?join)): abuse and moderation in the Jabber/XMPP federation
- [privacy@joinjabber.org (web chat)](https://anon.joinjabber.org/#/guest?join=privacy%40joinjabber.org) ([xmpp](xmpp:privacy@joinjabber.org?join)): privacy and security discussions, for users and server operators
- [translations@joinjabber.org (web chat)](https://anon.joinjabber.org/#/guest?join=translations%40joinjabber.org) ([xmpp](xmpp:translations@joinjabber.org?join)): translations for our resources, for worldwide accessibilty
- [sysadmin@joinjabber.org (web chat)](https://anon.joinjabber.org/#/guest?join=sysadmin%40joinjabber.org) ([xmpp](xmpp:sysadmin@joinjabber.org?join)): systems administration and reproducible infrastructure for our collective
- [website@joinjabber.org (web chat)](https://anon.joinjabber.org/#/guest?join=website%40joinjabber.org) ([xmpp](xmpp:website@joinjabber.org?join)): how to improve our website, and how you can help
