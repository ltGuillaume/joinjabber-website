+++
title = "Frequently Asked Questions"
template = "dummy_section.html"
+++

1. **[FAQ for operators](@/faqs/service/_index.md)**: questions for service operators
2. **[Advanced FAQ](@/faqs/advanced/_index.md)**: More detailed questions about Jabber/XMPP
3. **[Security FAQ](@/faqs/security/_index.md)**: Specific security/privacy related questions

## Questions for/from end-users {#faq-users}

### What is XMPP/Jabber? {#faq-users-what}

[XMPP](https://xmpp.org/) is an open standard (protocol) for communication, typically used for real-time instant messaging and group chats (even by huge evil corporations such as Facebook/Whatsapp). The XMPP protocol can be used to let users communicate across service providers (just like email), and this open federation is often called the Jabber federation. Jabber/XMPP is also a friendly community of users and developers, who value freedom of communication and privacy. Most of us are unpaid volunteers, and anyone is welcome to join the collective effort.

### Why should I care about federation in my instant messaging app? {#faq-users-federation}

The Jabber/XMPP federation is a decentralized network of servers that communicate with each other. This means that there is no central point of control or failure, making the network very resilient. As there is no [silo](https://en.wikipedia.org/wiki/Information_silo) holding your data and contacts hostage, [vendor lock-in](https://en.wikipedia.org/wiki/Vendor_lock-in) becomes impossible. Anyone can set up their own server and join the network, and from any account you can immediately communicate with million of users around the world.

### Why is there such a confusing variety of services and applications? {#faq-users-variety}

Similar to email, Jabber/XMPP is not operated by a single company so there's a variety of services and clients, each catering to different use-cases. JoinJabber.org was founded to make it easier to find the service and client that fits your use-case the best. As an independent volunteer run community we make our recommendations without any undue influence by commercial vendors. If our website/documentation is not helping you (yet), we encourage you to provide constructive criticism so we can improve it.

### Does JoinJabber.org offer Jabber/XMPP accounts? Where can I sign up for one? {#faq-users-accounts}

JoinJabber.org does not offer user accounts. Our Jabber/XMPP server is only used to host related group-chats and for bridging chat rooms to other networks such as IRC and Matrix. We decided early on that we do not want to become an irreplaceable part of the ecosystem, but would rather advertise other existing solutions.

### Is my data sold to advertisers? Am I the product? {#faq-users-privacy}

The Jabber/XMPP community is by and large run by enthusiastic volunteers without a profit motive, even while some companies make business out of it. The software we recommend is free and open-source software with transparent funding sources. Specific servers may infringe on your privacy, however it is not a common practice across the Jabber/XMPP ecosystem, and we do our best to ensure service providers advertised on [joinjabber.org](https://joinjabber.org/) are good-faith operators.

Additionally, some service providers may setup convenient features that you may or may not like. For example, [quicksy.im](https://quicksy.im/) was explicitly designed to let users register with their phone number, as they are doing on Whatsapp or Signal. If that's something you do not like, you are free to use any other service provider, and communicate with your friends on Quicksy. Finally, when you or your collective are running your own Jabber/XMPP servers, you are the ones in control and get to decide what is done with the data.

### Is it called XMPP or Jabber? Is there a difference? {#faq-users-xmppjabber}

This is a controversial topic but there is basically no difference other than the name that people prefer to use. Traditionally, XMPP refers to the standard protocol, while Jabber refers to the open federation of servers speaking the XMPP protocol. However, both may be used interchangeably.

“Jabber” as used colloquially is not to be confused with the “Jabber” trademark [owned](https://www.cisco.com/c/en/us/about/legal/trademarks.html) by Cisco Systems, Inc. since at least 2008. The trademark is somewhat related to XMPP in that Cisco’s various Jabber-named products are XMPP-based, albeit generally incompatible with the open XMPP/Jabber ecosystem.

### Does XMPP/Jabber support encryption? Are my conversations secure? {#faq-users-encryption}

There are two types of encryption we support and promote:

- transport security (network encryption)
- end-to-end encryption (content encryption)

Connections to/from/between servers are usually secured with [TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security), which is not bulletproof but protects users and servers from typical attackers on the network. Alternative transport mechanisms for the XMPP protocol such as Tor's onion services may provide better/additional security.

End-to-end encryption is supported by different extensions, most notably OMEMO encryption (derived from the Signal protocol) and PGP encryption. Many XMPP clients make OMEMO the default and as easy to use as possible without compromising your security. However, these implementations have not received a security audit. If a security audit is a requirement for you, please see the [Security FAQ](https://joinjabber.org/faqs/security).

### I read that XMPP/Jabber was replaced by the newer Matrix protocol {#faq-users-matrix}

XMPP and Matrix are two unrelated protocols with similar goals and properties.  Both are [federated](#faq-users-federation). Neither replaces the other and enhanced interoperability is a goal on both sides. XMPP is the long standing official IETF internet standard for real-time messaging, and allows anyone to contribute and expand the specifications. If you're unsure whether to use Matrix or XMPP (or both), you can find a somewhat opinionated comparison of each solution's strengths [here](https://joinjabber.org/faqs/advanced).


### Can I register an account with my phone number, so my contacts can find me easily? {#faq-users-phone}

There is a Jabber/XMPP service called [Quicksy](https://quicksy.im/) that does just that. However, this is usually considered an anti-privacy feature as phone numbers are easily linked to government IDs in most countries. Furthermore, mobile phones are a strong avenue for surveillance and actual attacks on the users (as demonstrated by [Facebook](https://www.independent.co.uk/life-style/gadgets-and-tech/news/facebook-phone-numbers-data-breach-privacy-a9092641.html), on [more than one occasion](https://appleinsider.com/articles/19/03/04/facebook-embroiled-in-yet-another-privacy-scandal-this-time-involving-your-phone-number)). However, we believe it is entirely fine for people to willingly give away their phone number, as long as the service [does not infringe on your friend's privacy](https://www.huffpost.com/entry/facebook-phonebook-contacts_n_924543).

### Can I make audio/video calls or conferences with XMPP/Jabber? {#faq-users-audiovideo}

The short answer is **yes**. There were clumsy attempts at audio/video conferencing a decade ago, but over the years the system has been refined. The new WebRTC standard based specifications for that are solid, although they are not widely implemented in clients yet. Video-conferencing requires significantly more server resources and is not widely supported on all servers. [Jitsi Meet](https://jitsi.org/jitsi-meet/) is a very popular free-software solution based on XMPP that can be deployed for video-conferencing. In fact, it's the solution used by the [matrix community](https://matrix.org/blog/2021/01/04/taking-fosdem-online-via-matrix).

### If XMPP/Jabber is so great, why is it not more popular? {#faq-users-popularity}

Most people use XMPP technology every day without even realizing it! XMPP forms the basis of many commercial messaging platforms (such as WhatsApp and Zoom) and is widely used in the gaming industry for multiplayer applications. Many large companies & organizations also utilize it for their internal communication. Unfortunately, few of these use-cases are open to the wider XMPP federation, and some that were open in the past decided to close this interoperability for mainly short term profit-oriented reasons.

The non-profit Jabber/XMPP ecosystem is growing every day, but until recently there was no community hub to promote non-profit use-cases. That's why we started the JoinJabber collective: provide up-to-date information for newcomers to the Jabber/XMPP ecosystem.
