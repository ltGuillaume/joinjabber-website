+++
title = "FAQ for server admins"
+++

## Questions aimed at service operators (self-hosted or not) {#faq-op}

### Can I run or rent a Jabber/XMPP service under my own custom domain(s)? {#faq-op-domain}

Yes, a single Jabber/XMPP server can easily serve multiple domains (virtual hosts) and there are projects that offer domain alias services similar to how some email providers do it. We may advertise such service providers in the *Collective* section of our [homepage](@/_index.md).

### Do I need an expensive server to run a XMPP/Jabber service myself? {#faq-op-resources}

No, Jabber/XMPP servers are extremely resource efficient. The lowest tier VPS from a hosting provider or a cheap ARM single board computer like the Rasberry Pi can easily host hundreds if not thousands of users simultaneously.

### Can I run a Jabber/XMPP server for my close friends? {#faq-op-friends}

Yes, in fact that is a very common use-case. Due to the federated nature of the network, you will be still able to communicate with anyone using Jabber/XMPP and you can help your less tech-savvy friends and family to enjoy a truly privacy preserving instant messaging system.

If you are looking for a complete Jabber/XMPP distribution (client and server) to host a small server of trusted peers, take a look at the [Snikket project](https://snikket.org/). Snikket servers provide out of the box the possibility for existing users to [invite new users](https://blog.prosody.im/great-invitations/) to join your server without opening registrations to the entire planet.

### Can I run a Jabber/XMPP server on a shared PHP website hosting? {#faq-op-php}

Sorry, this is generally not possible, because Jabber/XMPP servers must run continuously, and this is prevented by most shared hosting providers (who expect PHP apps to be used only for serving websites). This is partly the reason why there are no modern XMPP servers written using PHP even though in theory it is possible to write one in PHP.

### Should I open my server's registrations to the public? {#faq-op-public}

Hosting internet services comes with some responsibility and legal obligations. You should carefully consider whether you want to open up your server to strangers that no one you know can vet for, and might not share some principles with you. Open registrations (especially "in-band-registration") can be abused by spammers. If your server is abused by spammers, it is possible that other servers will block communications with your server.

However, given the federated nature of Jabber/XMPP, there is nothing wrong in preventing complete strangers from creating accounts on your server without your knowledge/approval, because they'll be able to create an account elsewhere and communicate with your users. As a middle-ground, you may consider setting up [invitations](https://blog.prosody.im/great-invitations/)-based registration. For multi-user chats, you may consider enabling anonymous logins so that users who do not have an account (yet) can join the conversation.

Finally, operating an open server is really cool. If you'd like to do that, please checkout some guides about [spam management](https://blog.prosody.im/simple-anti-spam-tips/).

### How can I federate my server with onion servers? {#faq-op-onions}

First, you should make sure Tor is installed on your server. Then, there's two ways to go about it. Prosody server has excellent Tor/onion integration with [mod_onions](https://modules.prosody.im/mod_onions.html).

If you don't want to use this module, or are not using Prosody, you may consider configuring Tor as [a transparent proxy](https://gitlab.torproject.org/legacy/trac/-/wikis/doc/TransparentProxy). Tor will answer to DNS queries about onion services (for example, on port 5352), announcing a record for a local address (for example in the `127.192.0.0/10` range), which iptables will forward through Tor's SOCKS5 proxy (for example, `127.0.0.1:9040`).

**Step 1:** Add to Tor config (usually `/etc/tor/torrc`):

```
VirtualAddrNetworkIPv4 127.192.0.0/10
VirtualAddrNetworkIPv6 [FE80::]/10
AutomapHostsOnResolve 1
TransPort 127.0.0.1:9040
TransPort [::1]:9040
DNSPort 5352
```

**Step 2:** Add in iptables config (usually `/etc/iptables/iptables.rules`):

```
-A OUTPUT -d 127.192.0.0/10 -p tcp -j REDIRECT --to-ports 9040
```

**Step 3**: Add in dnsmasq or other resolver's config (usually `/etc/dnsmasq.conf`)

Finally, configure your DNS resolver to use Tor's exposed transparent onion resolver as source of authority of `.onion` domain. For example, with dnsmasq you may simply add to `/etc/dnsmasq.conf`:

```
server=/onion/127.0.0.1#5352
```

### What are the legal requirements of hosting a Jabber/XMPP service? {#faq-op-legal}

Whether you are serving users from the European Union or not, you should be aware of the data protection requirements of the [GDPR](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation), because they are good advice for any kind of service. More specific regulations may apply depending on where the server is hosted, and the system administrator's country of residence. We currently do not provide more specific advice in this FAQ, though contributions are welcome.

### What other concerns should I have? {#faq-op-concerns}

Contrary to profit-oriented services, we believe running a XMPP server should preferably not be a goal by itself. Services / tools tend to be more sustainable if they serve a specific need or community instead of an unspecified general public. Also keep in mind that it is always better to have more than one administrator of the service, so that in case something might happen to you, someone else can easily take over.