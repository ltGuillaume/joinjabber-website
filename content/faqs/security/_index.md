+++
title = "Security related FAQ"
+++

## Security {#faq-sec}

### How can I protect myself from my server? {#faq-sec-protect}

Whatever level of trust you place in your server operator, you may take additional measures to protect your privacy. For example, reaching your server over [Tor](https://torproject.org/) or a [VPN](https://en.wikipedia.org/wiki/VPN) will protect your geolocation from your server. Additionally, encrypting your messages with [OMEMO](https://en.wikipedia.org/wiki/OMEMO) or [PGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy) will protect the content of your communications from your server, and the server of the person.

### How can a malicious server impact me? {#faq-sec-server}

A malicious server is a big deal, as with other federated networks (like [the Fediverse](https://en.wikipedia.org/wiki/Fediverse) or email). A malicious server can:

- read your unencrypted messages
- see the list of your contacts and group chats (and other non-chat groups/communities)
- read a history of your previously received/sent messages (if MAM is enabled on the server, and usually only for a certain time), and read your new incoming messages
- prevent you from receiving messages, or sending messages
- send messages to other servers/users impersonating you

This list may sound scary at first, but this security model is similar in most federated networks. The concept of federated networks is a server is responsible for many users and may act on their behalf. That's why we promote good security practices for the server, and in practice compromised email or Jabber/XMPP servers are extremely rare.

### What are other threats I should be aware of? {#faq-sec-other}

If your server is not compromised, you should still be aware that:

- your correspondent's server may be compromised, in which case your unencrypted communications with this correspondent may still be inspected/modified/blocked
- some public services (like chat rooms) publicize every member's address ; in this case, you may receive unsolicited messages from other users even after you leave the chatroom (in pseudonymous rooms, other users can only contact you as long as you are in the room)
- pseudonymous public services will still reveal your XMPP user address (JID) to their operators and administrators; additionally, chat servers implementing [occupant-id (XEP-0421)](https://xmpp.org/extensions/xep-0421.html) will give you a unique ID for every room, even if you join under a different nickname, so other users may recognize you (but not identify you) ; if you'd like to protect against that, you may create another Jabber/XMPP account to interact with such services.
- use of vcard avatar images can identify you across multiply pseudonymous group-chats even if using different names in each one
- most clients publish presence information (`online`, `away` status messages) by default, which may be used to correlate different identities of yours (eg. who come online/offline at the same time) or to spy on your schedule ; this can be easily disabled in most Jabber/XMPP clients, so don't hesitate to tell your client developers privacy should be the default!
- a passive observer on the network may record the network activity between you and your service provider, and try to correlate your network activity with your pseudonymous identity
- a passive observer on the network may discover you are connecting to a specific service provider by snooping on your DNS requests and/or TLS SNI headers
- an active attacker on the network can try:
  - a [downgrade attack](https://en.wikipedia.org/wiki/Downgrade_attack) to force less-secure communications with your service provider
  - to impersonate your service provider, with a TLS certificate approved by a well-established [Certificate Authority](https://en.wikipedia.org/wiki/Certificate_authority)
- your Jabber/XMPP client may store some information unencrypted on your system, such as your username/password or a history of your messages ; most clients can be configured not to do that

### Should I chose a service provider specifically aimed at activists? {#faq-sec-server}

If you are taking part in political dissent for social change, you may consider acquiring an account on a self-organized, activist-friendly hosting cooperative. These will usually ignore foreign legal requests to give away your data, but be aware they may still be compelled by their own local authorities to rat on users. Also, you should be aware radical servers promote security of social struggles, but will actively ban fascist sympathisers, crypto-scammers and other users engaged in harmful behavior for our communities. Script kiddies of the world, please run your own servers, you may learn a thing or two.

When using a non-profit service provider who does not take extra measures to protect their users' privacy, please keep in mind they are usually run by small communities, who may choose to collaborate with law enforcement agencies as they may not have the resources to fight in court and provision new servers in case of hardware seizure.

Finally, there may be advantages to having an account on a popular server. Depending on your threat model, an account on a big, popular server may help hide your metadata in the masses of connections to/from that server.

### Security audits {#faq-sec-audit}

Most of the Jabber/XMPP ecosystem has not been audited for security. Conversation's encrypted OMEMO chats were [audited in 2016](https://conversations.im/omemo/audit.pdf). On the server side, [prosody](https://prosody.im/) and [ejabberd](https://ejabberd.im/) don't have a public security audit available, but have very little vulnerabilities (apart from DOS vectors) found over the years ([ejabberd](https://www.cvedetails.com/vulnerability-list/vendor_id-4455/product_id-7709/Process-one-Ejabberd.html), [prosody](https://www.cvedetails.com/vulnerability-list/vendor_id-11422/Prosody.html)).

If a security audit is important to you, feel free to provide or finance security audits for parts of the ecosystem. Notably, some [prosody server](https://prosody.im/) developers have expressed the desire to be audited. If a comprehensive security audit is a requirement to you, and you don't have skills/resources to contribute that, please consider using alternatives to the Jabber/XMPP ecosystem such as [Signal](https://signal.org/) (last audit in 2017), [Briar](https://briarproject.org/) (last [audit in 2017](https://briarproject.org/news/2017-beta-released-security-audit/)) or [Threema](https://threema.ch/) (last audit 2020).

### Who develops Jabber/XMPP? Will it be blocked in my country? {#faq-users-block}

The XMPP/Jabber ecosystem is developed and run by a global network of volunteers and small organizations, although there are also big multinationals and governments relying on it. As of writing this, most of the free XMPP ecosystem volunteers reside in central Europe, but there are contributors from all over the world. While it is highly unlikely that a government would block the whole XMPP protocol (which is used for many things), it is entirely possible that they block specific servers, or block client connections to foreign servers, as is the case with many services going through China's [Great Firewall](https://en.wikipedia.org/wiki/Great_Firewall) for instance.

However, even though it is unlikely that Jabber/XMPP is blocked by your government, it is entirely possible that they monitor and record your connections, which is information that may be used against you.

In the most extreme cases, it is possible for a network operator (or government order) to block the Jabber/XMPP network entirely. In this case, using censorship-circumvention mechanisms like [Tor](https://torproject.org/) can help you stay in touch. However, please be aware that circumventing government censorship may be a criminal offense where you reside, and you may end up having trouble with your local authorities if they find out.
