+++
title = "JoinJabber meeting #2"
date = 2021-02-14
+++

This second meeting took place on [a pad](https://pad.lqdn.fr/p/joinjabber2) on February 4 2021 17:00 GMT, one week precisely after the first one. Some contributors were unavailable at that time, and only half a dozen people showed up. For this reason, it was decided that no important decision would be taken during this meeting: important, controversial matters are postponed for the next meeting, which will be prepared and announced longer in advance.

In this meeting, we took at look at what's been going on in the various **Working Groups** ([progress report](#points-progress)). Then, we talked about [communication](#points-comm-resources). Then, we had a point about access to [credentials](#points-security) for social media accounts, [what to do with our forums](#points-forum), and [how to organize the next meeting](#points-planning). Finally we had some quick [questions & answers](#questions) for whatever was not covered during this meeting.

The longer, potentially-controversial points about [Jabber vs XMPP](#points-comm-jabberxmpp) (the collective's name) and the [servers we recommend](#points-servers) have been discussed briefly, but no decision was taken. These two points are postponed for the next meeting. The meeting was closed about 4h45m after it started.


# Agenda {#agenda}

- [Progress report](#points-progress)
  - [Collective tasks](#points-progress-collective)
  - [Sysadmin working group](#points-progress-sysadmin)
  - [Website working group](#points-progress-website)
  - [Translations working group](#points-progress-translations)
  - [Bridging working group](#points-progress-bridging)
  - [Media working group](#points-progress-media)
- [Communication](#points-comm)
  - [Useful resources](#points-comm-resources)
  - [Suggestions for the website](#points-comm-websitesuggestions)
- [Security and credentials](#points-security)
- [Forums and mailing lists](#points-forum)
- [Planning the next meeting](#points-planning)
- [Questions and answers](#questions)
- [Postponed](#postponed)
  - [Recommended servers](#points-servers)
  - [Jabber or XMPP?](#points-comm-jabberxmpp)

# Points

In this section, we discuss individual points scheduled in the agenda.

## Progress report {#points-progress}

Before discussing other points, we take a moment to talk about what was achieved since the last meeting. In each subsection, we first address the working group's backlog from the last meeting, then introduce other initiatives taken by the working group since the last meeting.

This section consists of simple progress reports. It will be quick, and questions/concerns will be expressed in dedicated points in the agenda, or in the questions/answers at the end of this meeting. The TODOs for each Working Group include both a backlog of tasks, and the tasks added during this meeting.

### Collective tasks {#points-progress-collective}

In this point, we talk about the collective tasks decided in the last meeting, which were not assigned to specific working groups:

- [x] publish meeting #1's minutes: done [here](@/collective/meeting-1/index.md)
- [x] publish the announcement: done [here](@/blog/2021/announcement/index.md)
- [x] start a poll for the next meeting's date: done [here](https://forum.joinjabber.org/t/meeting-2-poll-for-the-date/28)
- [x] prepare the pad for the next meeting, adding points we could not discuss today to the agenda: done
- [ ] list other collectives we'd like to invite to the meeting
- [ ] prepare a tutorial on how to organize meetings and participate to them

**TODO:**

- [ ] list other collectives we'd like to invite to the meeting [here](https://pad.lqdn.fr/p/joinjabber-invitation)
- [ ] prepare a tutorial on how to organize meetings and participate to them

### Sysadmin Working Group {#points-progress-sysadmin}

**Backlog from the last meeting:**

- setup bridging solution proposed by the **Bridging Working Group**
- consider setting up [ConverseJS](https://conversejs.org/) for people to join our chatrooms and meetings from the web

**Notes since the last meeting:**

- [ConverseJS](https://conversejs.org/) is not packaged for Debian, nix or guix, we'll have to do a manual setup ; converseJS was not setup for the moment ; [xmpp-web](https://github.com/nioc/xmpp-web) is a potential, more lightweight alternative
- [Matterbridge](https://github.com/42wim/matterbridge) is not packaged for Debian or guix, but is packaged for nix (but the package is outdated) ; matterbridge was setup from source using golang from the Debian backports

**TODO:**

- configure prosody to support [BOSH](https://prosody.im/doc/setting_up_bosh) and [anonymous login](https://prosody.im/doc/anonymous_logins)
- consider whether to enable [mod_slack_webhooks](https://modules.prosody.im/mod_slack_webhooks.html) so that matterbridge can create ghost users instead of relaying messages through a single nickname (*highly experimental*)
- wait for more detailed matterbridge/ConverseJS/xmpp-web proposals and set them up

### Website Working Group {#points-progress-website}

**Backlog from the last meeting:**

- [x] open a blog section on the website
- [x] publish the first meeting meeting, and the announcement for our collective
- [ ] open a microblogging section on the website, and give access to the **Media Working Group**

**TODO:**

- [ ] open a microblogging section on the website, and give access to the **Media Working Group**
- [ ] integrate useful external resources
- [ ] list existing chatrooms on a page: [#chat](xmpp:chat@joinjabber.org?join), [#sysadmin](xmpp:sysadmin@joinjabber.org?join), [#website](xmpp:website@joinjabber.org?join), [#translations](xmpp:translations@joinjabber.org?join), [#bridging](xmpp:bridging@joinjabber.org?join), [#science](xmpp:science@joinjabber.org?join)
- [ ] display "construction signs" everywhere, letting our users know that everything is work-in-progress and all contributions are welcome

### Translations Working Group {#points-progress-translations}

**Backlog from the last meeting:**

- [ ] translate meeting #1 minutes
- [x] translate the announcement of the collective: french
- [ ] recruit more translators

**Notes since the last meeting:**

- there was an argument about French translations: should we use gender-neutral (inclusive) forms, or default to the State-approved variant of French?

**TODO:**

- [ ] Recruit more translators
- [ ] Formulate a proposal for inclusive language for the next meeting
- [ ] Translate meeting minutes #1 and #2

### Bridging Working Group {#points-progress-bridging}

**Backlog from the last meeting:**

- [x] explore bridging solutions for our multi-user chat: Matterbridge has been chosen
- [ ] make a concrete proposal to the **Sysadmin Working Group**

**Notes since the last meeting:**

- where to host Matrix account/chatroom?
  - [feneas.org](https://feneas.org/): they are really worried about the state of their sysadmin, and the lack of contributors there, so they would not recommend depending on them for anything
  - [aria-net.org](https://aria-net.org/) (Metronome project): they would be happy to host a chatroom and a bot account for us, and we can promote their Bifrost XMPP <-> Matrix gateway to the general public (though Bifrost is still beta)
- where to host IRC account/chatroom? Freenode? OFTC? ~chat?
- how to store and share user credentials? see **[Security and credentials](#points-security)** later in the agenda
- matterbridge was tested for this meeting (though not hosted on our infra), relaying discussions between meeting@joinjabber.org and #joinjabber on irc.tilde.chat ; it appears very functional

**TODO:**

- [ ] create matrix/IRC accounts with `bridging@joinjabber.org` recovery email
- [ ] make a concrete proposal to the **Sysadmin Working Group**

### Media Working Group {#points-progress-media}

**Backlog from the last meeting:**

- [ ] create accounts on Mastodon and Movim, and share credentials
- [ ] obtain credentials from the **Website Working Group** to publish on website in the microblogging section
- [ ] consider whether we should selfhost our own Movim instance, and if so get in touch with the **Sysadmin Working Group**

**Notes since the last meeting:**

- how to share credentials within the **Media Working Group**? see **[Security and credentials](#points-security)** in the agenda
- RSS to ActivityPub (Mastodon) bots were considered, several options found
  - [rss-to-activitypub](https://github.com/dariusk/rss-to-activitypub) (NodeJS)
  - [feed2toot](https://gitlab.com/chaica/feed2toot/) (Python)
  - [mastofeeds](https://gitlab.com/spla/mastofeeds) (Python)
- some services require email confirmation for signup: `media@joinjabber.org` was created for this purpose, and is an alias redirecting to current members of the **Media Working Group**

**TODO:**

- [ ] create accounts on Mastodon and Movim, and share credentials
- [ ] obtain credentials from the **Website Working Group** to publish on website in the microblogging section
- [ ] consider whether we should selfhost our own Movim instance, and if so get in touch with the **Sysadmin Working Group** with a concrete proposal

## Communication {#points-comm}

The point that was scheduled about whether we'd prefer to use `Jabber` or `XMPP` to refer to the Jabber/XMPP ecosystem has been postponed.

### Useful resources {#points-comm-resources}

What external resources do we want to advertise on our website? Some ideas:

- https://modernxmpp.org/
- https://snikket.org/
- https://homebrewserver.club/
- https://omemo.top/
- https://www.freie-messenger.de/sys_xmpp/ (German)
- https://cryptoflausch.de/ (German)
- https://search.jabber.network/
- https://observe.jabber.network/

Additionally, here's some good places to find news about Jabber/XMPP:

- [xmpp tag on Lemmy](https://lemmy.ml/c/xmpp)
- [XSF blog on xmpp.org](https://xmpp.org/blog.html)
- [Planet Jabber](https://planet.jabber.org/)

Ideas about how to incorporate those resources on the website will be formulated as part of the **Website Working Group**. They could be divided according to the intended audience: end-users, server operators, client/server developers.

### Website suggestions {#points-comm-websitesuggestions}

Some suggestions from the last meeting, and others formulated since then, say we should add on the site:

- [x] tutorials: done in [Tutorials section](@/tutorials/_index.md)
- [ ] FAQ: contributions welcome!
- [ ] Contributing guide

It is pointed out that our website doesn't advertise a license. Which license should we adopt? A copyleft license like Creative Commons BY-SA sounds good, but this specific point is postponed for the next meeting.

It's asked whether we should setup a wiki software (like [MediaWiki](https://www.mediawiki.org/wiki/MediaWiki)) on our server ; here's a list of pros & cons:

- **+** simple web interface for contributing without git knowledge
- **-** yet another application to maintain
- **-** we don't have any centralized authentication service, so it would be more usernames/passwords for people to remember (though 3rd party authentication could work)
- **-** redundant with the website which is already like a wiki (just needs a web editor)
- **-** contrary to the website (static files), a dynamic wiki cannot be redistributed on peer-to-peer networks (Bittorrent/IPNS/Freenet)

As an alternative, we could:

- use repository wikis on [our forge](https://codeberg.org/joinjabber/): however, it would make it harder to export the information to our website
- use Gitea's integrated web editor to have less-technical folks submit merge requests: however, Gitea currently doesn't have an integrated "fork-and-edit" workflow like Github/Gitlab does

Also, it has been suggested we could find design inspiration from one of the following sites:

- https://join.lemmy.ml/
- https://joinmastodon.org/
- https://www.gnu.org/software/guile/

We remind folks that the JoinJabber project is volunteer-run, and a volunteer webdesigner is welcome to help improve our website.

Some additional remarks were collected throughout the meeting. They have been added to the **Website Working Group**'s TODO.

## Security and credentials {#points-security}

How to share passwords to social media accounts (and others) ? How to prevent unauthorized access, while ensuring the passwords don't get lost over time? For passwords used by bots/services, we can keep them on the server. For passwords used by humans (eg. social media accounts), they can be kept in contributor's mailboxes (for the moment). We just setup dedicated aliases on our mail system:

- *media@joinjabber.org*: for social media accounts (used by humans)
- *bridging@joinjabber.org*: for bot/bridges credentials on other platforms (used by server)

These aliases will redirect mail to members of the corresponding working groups.

It was suggested to use a shared password manager. However, **Sysadmin Working Group** folks are unhappy to deploy yet another service before we have good support for the existing services. It was suggested, as an alternative, that passwords can be GPG-encrypted for the persons who need them.

## Forums and mailinglists {#points-forum}

We currently use [Discourse](https://discourse.org/) as forum, because it's the only solution we had found which worked without JavaScript in a web browser, and was also usable via email. However, not everything is so great about it:

- Discourse only supports one baseURL for each virtualhost, so we cannot serve the same forum over a `.onion` address ([ticket](https://codeberg.org/joinjabber/infra/issues/1))
- Discourse install is only supported as a Docker container, so we should make an Ansible role to support Discourse setup ([ticket](https://codeberg.org/joinjabber/infra/issues/5))
- Discourse email stack is crazy bad (see ticket above)
- Discourse is not based on XMPP protocol: while it's no reason not to use the software, a solution based on Jabber/XMPP would make us happier
- Discourse may not be the easiest to use with Jabber/XMPP authentication, or other federated authentication mechanisms

Usual solutions like Mailman and Redmine don't apply because they only do a forum or a mailing-list, but are really bad at doing both at the same time. Three solutions based on Jabber/XMPP were explored to replace the current forum:

- [Movim](https://movim.eu/) was full of tiny (harmless) bugs, and is clearly not intended to be used as a forum (a maintainer told us not to even try)
- [Salut-à-toi](https://salut-a-toi.org/) has not been tested yet, but on paper looks better suited, and the maintainer has shown strong interest for us using his project ; sàt forums currently only work from web interface (libervia) or other sàt frontends (so www + Jabber/XMPP), but the maintainer already plans to implement full email integration
- standard multi-user chats with a web-interface for read-only logs, with search feature ; this solution does not work so well because there are no threads/categories (information discovery is hard)

We'll start a test instance of salut-à-toi for the next meeting, so people can test it and collect feedback for the maintainers of the project, who are very interested to have real-world feedback.

## Planning the next meeting {#points-planning}

So far, we've asked people to vote for the meeting date on the forums. However, it requires to create an account in order to vote, which some people were relunctant to do. We could use a public poll system like [framadate](https://framagit.org/framasoft/framadate/framadate). This will be experimented for the next meeting.

Also, up until now most of the work before and after a meeting has been done by a single person. It doesn't take too much time (2h before meeting, 2h after), and certainly requires no specialized skills (typos are ok, too). Ideally more people would get involved and nobody would have to do it for every meeting. This volunteer will still prepare the next meeting, but will work on a tutorial on how to organize the meetings so they can be replaced by anyone.

How do we announce the next meeting? Currently, the same person has been more or less spamming many jabber-related MUC with announcements of our meeting. Some ideas:

- [x] the chat@joinjabber.org & meeting@joinjabber.org channel topics
- [ ] a dedicated MUC for announcements? (maybe annoying to join yet another channel)
- [x] social media accounts from the **Media Working Group**
- [x] a header on the website, as we did for the first meeting?

Should the meeting take place in chat@ or meeting@? It makes sense to divide discussions into smaller channels, but it also splits an already-small community. Having long debates like a meeting in chat@joinjabber.org would probably not be so welcoming to newcomers, who may feel overwhelmed with too much information and just leave.

Which collectives and other projects should we invite to join our meetings? How do we reach out to them? What's our message? We could setup a poll to get feedback from more client/server developers, and server operators:

- would they like to receive information/invitations about the JoinJabber project?
- what would they like to see in the JoinJabber project? what kind of efforts could we mutualize?
- would they like to be advertised on the JoinJabber homepage?
- how would they like to be presented by our collective? What kind of use case do they represent?
- what are they doing for privacy and security, if they have any concern for it at all? what could be improved? what could they use some help with?
- what are they doing for translations? what could be improved?

tofu and eevvoor will make a complete proposal based on these, and submit it for public review in the chatroom.

# Questions and answers {#questions}

## Avoid becoming a central point?

**Question:** How do we avoid becoming a central infrastructure in the ecosystem? How do we prevent ourselves from becoming a Single Point Of Failure, despite providing services for many projects?

**Answer:** For the moment, we're focusing on providing information only so the question is not that relevant. However, our goals state we should provide additional services to the ecosystem (eg. translation system, software repositories).

In the case of specific services (weblate, forums), we should ensure our infrastructure is in essence (as much as is practical) forkable at any time by following our sysadmin recipes. This should already be the case. Additionally, and as specified by [GDPR](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation), we should enable any member/project using our services to export their data to move it to a new location. Finally, maybe we could only accept to host services for projects who provide their own domain, so they remain autonomous in the future?

In the case of software repositories, it is a harder problem. In case our infrastructure is compromised, we do not want to end up serving malware on our repositories. To our knowledge, only [GNU guix](https://guix.gnu.org/) could mitigate this problem by:

- enforcing valid PGP signatures on all commits, through [channel introductions](https://guix.gnu.org/en/blog/2020/securing-updates/)
- enforcing reproducible builds, and enabling clients to [challenge build caches](https://guix.gnu.org/manual/en/html_node/Invoking-guix-challenge.html) to ensure they agree on the produced builds

## Crowdfunding

**Question:** Do we plan to take an active part in crowdfunding of features for XMPP servers/clients?

**Answer:** Yes, as decided in our goals from last meeting. We will not start crowdfunding campaigns on our own, but can advertise existing ones. Though, for the moment we received no concrete proposal for a crowdfunding campaign to support.

## iOS experience

**Question:** How to onboard users using iOS (with awkward-to-use clients)?

**Answer:** Is something really wrong with Siskin or Monal? Isn't that just an outdated notion that iOS clients are bad? Please let us know more concrete feedback.

## Selfhosting responsibilities

**Question:** Please make new (and old users) clearly aware of what it means to self-host a server and the responsibility you take on as an admin. For an overview of how operators can abuse their powers, see [this article](https://infosec-handbook.eu/blog/xmpp-aitm/#summary)

**Answer:** This is true of any service provided to end-users, more or less. We would like to provide operators and end-users with more information about this, as well as [GDPR](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation) compliance. Contributions are welcome.

## Donations

**Question:** How to accept donations?

**Answer:** We don't accept money for donations, but everyone is welcome to donate time, energy and ideas. You may also donate money or machines to our host [Fosshost](https://fosshost.org/) or to any of the upstream projects and service providers we're recommending.

## Science WG

**Question:** It is proposed to consider Jabber/XMPP as a platform for scientists, and have a dedicated use case on our website for that.

**Answer:** A **Science Working Group** may be constituted on the [science@joinjabber.org](xmpp:science@joinjabber.org?join) chatroom, and a more concrete proposal will be further discussed in the next meeting.

## School WG

**Question:**  A **School Working Group** could be constituted to promote remote-learning and video-conferencing solutions like Jitsi.

**Answer:** Nobody present at the meeting would like to start this effort, but some would gladly welcome new contributors for this and maybe take part in the future. A guide in German [already exists](https://www.freie-messenger.de/dateien/bildungstraeger/Virtuelles_Klassenzimmer.PDF) about this question. We could invite its author to join our discussions.

## Multi-user cooperation over XMPP

Not exactly a question, but some projects are mentioned for multi-user cooperation with Jabber/XMPP:

- [salut-à-toi](https://salut-a-toi.org/) for forums and federated software forge
- [saros](https://www.saros-project.org/) for real-time coding with up to 5 persons
- [jsXC](https://www.jsxc.org/) provides Nextcloud/Sogo integration for sharing files & such

If some people would like to mention such use cases on our website, while warning end-users they are niche and potentially buggy, contributions are welcome.

# Postponed

## Recommended servers {#points-servers}

We currently recommend some service providers on our homepage, however the criteria for inclusion is not clear. We could obviously add more providers, but there was also concerns that some providers could be using Google's infamous, user-hostile RECAPTCHA, or the just-as-much infamous Cloudflare. We mentioned at the last meeting we'd like to setup more objective criteria for deciding whether we want to recommend a server.

Depending on the user's intended use case, different criteria may apply:

- [ ] **not-for-profit**: the service provider is a non-profit association or a workers cooperative
- [ ] **no-3rd-party**: the user-facing services setup by the service provider do not include resources/calls to 3rd parties, such as analytics, CAPTCHAs or CDNs
- [ ] **[bus factor](https://en.wikipedia.org/wiki/Bus_factor)**: the services are provided by one than more person, with safeguards in place in case one sysadmin could not contribute to the project anymore
- [ ] **sustainability**: the economic model is understandable, and there is transparency on operating costs and donations
- [ ] **privacy**: the privacy policy is understandable by users, and sounds reasonable to us (up for interpretation)

First, we should decide what use cases we'd like to support, then we'll decide what specific criteria apply to any:

- [ ] **Personal**: for friends and families, with reasonable privacy, but a preference for sustainability (service should still be there in 10 years)
- [ ] **Collective**: for collectives and cooperatives, bring your own domain and administrate your own virtual host, with a preference for sustainability and reliability ; additionally, we may consider it a requirement for this use case to provide an official bill/quote for services
- [ ] **Pseudonymous**: for activists, with preference for security over durability ; maybe this use case can be renamed (eg. "Activist Friendly")

Some lists already exist to reference Jabber/XMPP service providers:

- https://list.jabber.at/
- https://novaburst.tilde.team/services/xmpp-servers.html
- [ejabberd](https://the-federation.info/ejabberd) and [prosody](https://the-federation.info/prosody) pages on [the-federation.info](https://the-federation.info/)

Additionally, some programs enable discovery/evaluation of services based on programmatic tests:

- https://invent.kde.org/melvo/xmpp-providers 
- the-federation.info is populated by the [nodeinfo2 schema](https://modules.prosody.im/mod_nodeinfo2.html) ; we could rehost a [new the-federation instance](https://github.com/thefederationinfo/the-federation.info) dedicated to Jabber/XMPP
- list.jabber.at is populated by  [django-xmpp-server-list](https://github.com/mathiasertl/django-xmpp-server-list); we could rehost an instance (see [ticket](https://codeberg.org/joinjabber/website/issues/8)

## Communications

### Free license {#points-comm-license}

It was pointed out during our second meeting that we don't have a license yet on our website. All our production should definitely be copyleft.

### joinjabber or joinxmpp? {#points-comm-jabberxmpp}

-  Jabber is trademarked by Cisco and usually associated with the old xmpp of the pre 2010 times or worse some Cisco corporate inhouse chat people dumped for Slack
- XMPP has a uphill battle against tech people associating it with bad "Jabber" experiences in the past. Hence modern XMPP should distance itself as much as possible from the old Jabber
- joinjabber.org should redirect to joinxmpp.org not the other way around like it is now
- joinxmpp.org is fine for what it is i.e. sharing a link that explains modern xmpp and its clients, actual end-user adoption is driven by client and server recommendations, not so much the protocol
- People say Jabber is easier to remember and pronounce than XMPP
