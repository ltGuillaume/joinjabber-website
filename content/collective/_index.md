+++
title = "About the JoinJabber collective"
+++

In this section, you will find the [minutes](https://en.wikipedia.org/wiki/Minutes) of our meetings. If you're interested in our goals, you can find them [here](@/collective/about/goals/index.md).
