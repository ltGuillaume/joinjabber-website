+++
title = "Objectifs du collectif"
+++

Quand nous avons [fondé notre collectif](@/blog/2021/announcement/index.fr.md), nous nous sommes fixé un certain nombre d'objetifs. Sur cette page, tu trouveras la liste mise-à-jour de ces objectifs, raffinés au cour du temps. Ces objectifs ont été débattus en Anglais pendant nos réunions, et les contributions sont bienvenues.

TODO
