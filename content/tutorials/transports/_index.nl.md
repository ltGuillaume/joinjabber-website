+++
title = "Transports"
template = "dummy_section.html"
+++

In het Jabber/XMPP-ecosysteem zijn "transports" de middelen om via je client verbinding te maken met andere protocollen.

1. **[IRC](@/tutorials/transports/irc/index.nl.md)**
