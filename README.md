This repository contains the source of the [joinjabber.org](https://joinjabber.org/) website. It requires [zola](https://getzola.org/) v0.12 to build the website.

# Building

If you want to build the site for joinjabber.org, `zola build` should do it. It will output the built website in the `public/` folder. You can change the default base URL with the `-u` flag, and the output destination with the `-o` flag, like this:

```
zola build -u "https://test.joinjabber.org" -o /var/www/
```

# Translations

Translations for content are located in dedicated Markdown files. Such translations are enabled with the config `languages` field. So if you have a file called `content/gnulinux/index.md`, its translations will be in `content/clients/gnulinux/index.CODE.md` where code is a language code as enabled in the config. For example, `content/clients/gnulinux/index.fr.md` for the french translation of the page.

Translations for template strings are located in the `config.toml` file in the `translations` table.

# Modifying content

Most of the content is configured in the config file. This allows to use Markdown files only for translations, and to provide a consistent design across languages.

## Platforms

To add a platform, you first need to enable it in the config. In the `extra.downloads` table, add a key containing a list of corresponding repositories, like `platform = ["repo1", "repo2"]`. This allows to filter download options for multi-platform software.

Then, you can create a folder for the platform in the `content/clients/` folder. It needs to have the same name as the key defined in `config.extra.downloads`. The Markdown pages inside this folder (index.md and translations) should have the following TOML frontmatter:

```
+++
title = "Your Platform name"
template = "client.html"
[extra]
platform = "platform_key" # same as the folder name
clients = [ "software1", "software2" ] # list of client software available on this platform
+++
```

## Client software

A client is a table in the `extra.clients` table in the site config. A client `c` has the following keys:

- website: URL for the project website
- forge: URL for the project's forge
- bugtracker: URL for the project's bugtracker
- logo: path relative to `static/clients/c/`
- screenshot: path relative to `static/clients/c/`
- platforms: table with a platform->boolean mapping
- download: table with a repository->package mapping
- features: table with a feature->boolean mapping

### Features

Client features are simple boolean flags to enable easy comparison of software. These features are declared in config's `extra.clientfeatures` set, as a simple list of strings. Then, each client `c` has a `extra.clients.c.features` table with a mapping of those features to a boolean. To add a feature, add it in `extra.clientfeatures`, then add it to every client's features.

## Repositories

Repositories contain instructions about how to download and setup client software. A repository may be used on several platforms, as defined in config's `extra.downloads`. A repository's output on a platform/client page is defined in `templates/download.html`, in the `download` macro.

Each client's download option has a value associated with a repository. This is usually the package name on said repository, but can be anything the download macro will use to produce setup instructions.

## Servers

For now servers are only filtered by usecase, not by country/language, although this could change in the future. A usecase contains a table (mapping) of servers, each containing a `website` URL and a potential `logo` (relative to the `static/servers/` folder).
